/*
 Author::name("Talles H");
 License::type("HaveFun");
 */

#ifndef MK_VERBOSEO_VA_ARGS_COUNT
#define MK_VERBOSEO_VA_ARGS_COUNT
int mk_verbose_va_args_count = 0;
#endif // __MK_VERBOSEO_VA_ARGS_COUNT__

#ifdef __cplusplus  

    #define get_name(x) #x
    #define typename(x)
    #define chk_pointer(x)
    #define chk_fmt(x) 
    #define print_mem(x) 

#else


#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>  //[u]int[num]_t
#include <math.h>    //float_t double_t
#include <stdbool.h> //bool
#include <float.h>   //FLT_EVAL_METHOD 

/* Get var name as a string. */
#define get_name(x) #x

/* Return var type in string form. */
#define typename(x) _Generic((x),\
  /*NO POINTER TYPES*/\
_Bool          : "BOOL"          , signed char             : "CHAR"   ,        \
char           : "CHAR"          , unsigned char           : "UCHAR"  ,        \
short int      : "S_INT"         , unsigned short int      : "S_UINT" ,        \
int            : "INT"           , unsigned int            : "UINT"   ,        \
long int       : "L_INT"         , unsigned long int       : "L_UINT" ,        \
long long int  : "LL_INT"        , unsigned long long int  : "LL_UINT",        \
float          : "FLOAT"         , double                  : "DOUBLE" ,        \
long double    : "L_DOUBLE",                                                   \
  /*POINTER TYPES  */\
_Bool*        : "BOOL[pointer]"  , signed char*           : "CHAR[pointer]"   ,\
char*         : "CHAR[pointer]"  , unsigned char*         : "UCHAR[pointer]"  ,\
short int*    : "S_INT[pointer]" , unsigned short int*    : "S_UINT[pointer]" ,\
int*          : "INT[pointer]"   , unsigned int*          : "UINT[pointer]"   ,\
long int*     : "L_INT[pointer]" , unsigned long int*     : "L_UINT[pointer]" ,\
long long int*: "LL_INT[pointer]", unsigned long long int*: "LL_UINT[pointer]",\
float*        : "FLOAT[pointer]" , double*                : "DOUBLE[pointer]" ,\
long double*  : "L_DOUBLE[pointer]",                                           \
default: "undefined")

/* Check if a var is pointer type. */
#define chk_pointer(x) _Generic((x), \
_Bool*        : 1, signed char*           : 1,\
char*         : 1, unsigned char*         : 1,\
short int*    : 1, unsigned short int*    : 1,\
int*          : 1, unsigned int*          : 1,\
long int*     : 1, unsigned long int*     : 1,\
long long int*: 1, unsigned long long int*: 1,\
float*        : 1, double*                : 1,\
long double*  : 1, void*                  : 1,\
default: 0)

#define chk_fmt(x) _Generic((x), \
_Bool         : "%s", signed char           : "%c",\
char          : "%c", unsigned char         : "%c",\
short int     : "%h", unsigned short int    : "%hu",\
int           : "%d", unsigned int          : "%u",\
long int      : "%ld", unsigned long int     : "%lu",\
long long int : "%lld", unsigned long long int: "%llu",\
float         : "%F", double                : "%F",\
long double   : "%lF", \
_Bool*        : "%s", signed char*           : "%c",\
char*         : "%s", unsigned char*         : "%c",\
short int*    : "%h", unsigned short int*    : "%hu",\
int*          : "%d", unsigned int*          : "%u",\
long int*     : "%ld", unsigned long int*     : "%lu",\
long long int*: "%lld", unsigned long long int*: "%llu",\
float*        : "%F", double*                : "%F",\
long double*  : "%lF", \
default: 0)



/* Print more verbose status about var */
#ifndef MK_VERBOSE_MEMORY_BUFFER
#define MK_VERBOSE_MEMORY_BUFFER
char mk_verbose_memory_buffer[50];
#endif //MK_VERBOSE_MEMORY_BUFFER

//#pragma GCC diagnostic ignored "-Wpointer-type-mismatch"

#define print_mem(x)                                    \
  snprintf(mk_verbose_memory_buffer, 50, "   | %s{%s}::%s\n",\
    get_name(x), typename(x),                           \
    (chk_pointer(x)==1?"->%p": chk_fmt(x))              \
  );                                                    \
  printf( mk_verbose_memory_buffer, x);                 \
  printf("   +-----  size[%ld]::mem[%p]\n",                      \
    sizeof(x), (void*)&x                                \
  )


//#define arg_count(...)  (sizeof((int[]){__VA_ARGS__})/sizeof(int))
#define _(x) print_mem(x) ; mk_verbose_va_args_count += 1;
#define mk_mem_verbose(title,...) \
  printf("[%s]\n",title); \
  __VA_ARGS__    \
  printf("   +[end]\n")
 
#endif //CC

int main()
{
  int test = 2;
  int *dtest = &test;
  print_mem(test);
  print_mem(dtest);

 
  mk_mem_verbose("Struct X",
    _(test)
    _(dtest)
  );

  #ifdef TEST_VAR_TYPES
  /*BOOL*/
  _Bool _b = true;
  bool  b = true;
  printf("BOOL TYPE::\n"
    "\tname\ttype\t\t\tsize\n"
    "\t%s  \t[ %s ] \t\t[ %ld ]\n"
    "\t%s  \t[ %s ] \t\t[ %ld ]\n",
    get_name(_b), typename(_b), sizeof(_b),
    get_name(b) , typename(b) , sizeof(b));  

  _Bool *_bp;
  bool  *bp;
  printf("BOOL POINTER::\n"
    "\tname\ttype\t\t\tsize\n"
    "\t%s  \t[ %s ] \t[ %ld ]\n"
    "\t%s  \t[ %s ] \t[ %ld ]\n\n",
    get_name(_bp), typename(_bp), sizeof(_bp),
    get_name(bp) , typename(bp) , sizeof(bp));  
  
  
  /*CHAR*/
  int8_t        i8_t = 'a';
  char          c    = 'a';
  signed   char sc   = 'a';
  unsigned char usc  = 'a';
  printf("CHAR TYPE::\n"
    "\tname\ttype\t\t\tsize\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t\t[ %ld ]\n\n",
    get_name(i8_t), typename(i8_t), sizeof(i8_t),
    get_name(c)   , typename(c)   , sizeof(c)   ,
    get_name(sc)  , typename(sc)  , sizeof(sc)  ,
    get_name(usc) , typename(usc) , sizeof(usc)); 

  int8_t        *i8_tp;
  char          *cp   ;
  signed   char *scp  ;
  unsigned char *uscp ;
  printf("CHAR POINTER::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s  ]\t[ %ld ]\n"
    "\t%s  \t[ %s  ]\t[ %ld ]\n"
    "\t%s  \t[ %s  ]\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t[ %ld ]\n\n",
    get_name(i8_tp), typename(i8_tp), sizeof(i8_tp),
    get_name(cp)   , typename(cp)   , sizeof(cp)   ,
    get_name(scp)  , typename(scp)  , sizeof(scp)  ,
    get_name(uscp) , typename(uscp) , sizeof(uscp)); 
  
  
  /*INT*/
  int16_t   i16_t  = 1;
  int32_t   i32_t  = 1;
  int64_t   i64_t  = 1;
  intmax_t  imax_t = 1;
  uint16_t  ui16_t = 1;
  uint32_t  ui32_t = 1;
  uint64_t  ui64_t = 1;
  uintmax_t uimax_t= 1;
  printf("INT_T::\n"
    "\tname\ttype\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s    ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s ]\t\t[ %ld ]\n"
    "\t%s\t[ %s   ]\t\t[ %ld ]\n"
    "\t%s\t[ %s ]\t\t[ %ld ]\n"
    "\t%s\t[ %s ]\t\t[ %ld ]\n\n",
    get_name(i16_t)  , typename(i16_t)  , sizeof(i16_t) ,
    get_name(i32_t)  , typename(i32_t)  , sizeof(i32_t) ,
    get_name(i64_t)  , typename(i64_t)  , sizeof(i64_t) ,
    get_name(imax_t) , typename(imax_t) , sizeof(imax_t),
    get_name(ui16_t) , typename(ui16_t) , sizeof(ui16_t),
    get_name(ui32_t) , typename(ui32_t) , sizeof(ui32_t),
    get_name(ui64_t) , typename(ui64_t) , sizeof(ui64_t),
    get_name(uimax_t), typename(uimax_t), sizeof(uimax_t)); 
  
  int16_t   *i16_tp  ;
  int32_t   *i32_tp  ;
  int64_t   *i64_tp  ;
  intmax_t  *imax_tp ;
  uint16_t  *ui16_tp ;
  uint32_t  *ui32_tp ;
  uint64_t  *ui64_tp ;
  uintmax_t *uimax_tp;
  printf("INT_T POINTER::\n"
    "\tname\ttype\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s    ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s ]\t[ %ld ]\n"
    "\t%s\t[ %s   ]\t[ %ld ]\n"
    "\t%s\t[ %s ]\t[ %ld ]\n"
    "\t%s[ %s ]\t[ %ld ]\n\n",
    get_name(i16_tp)  , typename(i16_tp)  , sizeof(i16_tp) ,
    get_name(i32_tp)  , typename(i32_tp)  , sizeof(i32_tp) ,
    get_name(i64_tp)  , typename(i64_tp)  , sizeof(i64_tp) ,
    get_name(imax_tp) , typename(imax_tp) , sizeof(imax_tp),
    get_name(ui16_tp) , typename(ui16_tp) , sizeof(ui16_tp),
    get_name(ui32_tp) , typename(ui32_tp) , sizeof(ui32_tp),
    get_name(ui64_tp) , typename(ui64_tp) , sizeof(ui64_tp),
    get_name(uimax_tp), typename(uimax_tp), sizeof(uimax_tp)); 
  
  
  short                  s   = 1;
  long                   l   = 1;
  long long              ll  = 1;
  unsigned short         us  = 1;
  unsigned long          ul  = 1;
  unsigned long long     ull = 1;
  printf("ONLY_SIGNAL::\n"
    "\tname\ttype\n"
    "\t%s\t[ %s   ]\t\t[ %ld ]\n"
    "\t%s\t[ %s   ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s ]\t\t[ %ld ]\n\n",
    get_name(s)  , typename(s)  , sizeof(s),
    get_name(l)  , typename(l)  , sizeof(l),
    get_name(ll) , typename(ll) , sizeof(ll),
    get_name(us) , typename(us) , sizeof(us),
    get_name(ul) , typename(ul) , sizeof(ul),
    get_name(ull), typename(ull), sizeof(ull)); 
    
  short              *sp  ;
  long               *lp  ;
  long long          *llp ;
  unsigned short     *usp ;
  unsigned long      *ulp ;
  unsigned long long *ullp;
  printf("ONLY_SIGNAL POINTER::\n"
    "\tname\ttype\n"
    "\t%s\t[ %s   ]\t[ %ld ]\n"
    "\t%s\t[ %s   ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s ]\t[ %ld ]\n\n",
    get_name(sp)  , typename(sp)  , sizeof(sp),
    get_name(lp)  , typename(lp)  , sizeof(lp),
    get_name(llp) , typename(llp) , sizeof(llp),
    get_name(usp) , typename(usp) , sizeof(usp),
    get_name(ulp) , typename(ulp) , sizeof(ulp),
    get_name(ullp), typename(ullp), sizeof(ullp)); 
  
   
  short int              s_i = 1;
  int                    i   = 1;
  long int               li  = 1;
  long long int          lli = 1;
  signed short int       ssi = 1;
  signed int             si  = 1;
  signed long int        sli = 1;
  signed long long int   slli= 1;
  printf("SIGNED INT::\n"
    "\tname\ttype\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s    ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s    ]\t\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t\t[ %ld ]\n"
    "\t%s\t[ %s ]\t\t[ %ld ]\n\n",
    get_name(s_i) , typename(s_i) , sizeof(s_i),
    get_name(i)   , typename(i)   , sizeof(i)  ,
    get_name(li)  , typename(li)  , sizeof(li) ,
    get_name(lli) , typename(lli) , sizeof(lli),
    get_name(ssi) , typename(ssi) , sizeof(ssi),
    get_name(si)  , typename(si)  , sizeof(si) ,
    get_name(sli) , typename(sli) , sizeof(sli),
    get_name(slli), typename(slli), sizeof(slli)); 
    
  short int            *s_ip ;
  int                  *ip   ;
  long int             *lip  ;
  long long int        *llip ;
  signed short int     *ssip ;
  signed int           *sip  ;
  signed long int      *slip ;
  signed long long int *sllip;
  printf("SIGNED INT POINTER::\n"
    "\tname\ttype\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s    ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s    ]\t[ %ld ]\n"
    "\t%s\t[ %s  ]\t[ %ld ]\n"
    "\t%s\t[ %s ]\t[ %ld ]\n\n",
    get_name(s_ip) , typename(s_ip) , sizeof(s_ip),
    get_name(ip)   , typename(ip)   , sizeof(ip)  ,
    get_name(lip)  , typename(lip)  , sizeof(lip) ,
    get_name(llip) , typename(llip) , sizeof(llip),
    get_name(ssip) , typename(ssip) , sizeof(ssip),
    get_name(sip)  , typename(sip)  , sizeof(sip) ,
    get_name(slip) , typename(slip) , sizeof(slip),
    get_name(sllip), typename(sllip), sizeof(sllip)); 



  unsigned short int     usi = 1;
  unsigned int           ui  = 1;
  unsigned long int      uli = 1;
  unsigned long long int ulli= 1;
  printf("UNSIGNED INT::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s    ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t\t[ %ld ]\n\n",
    get_name(usi) , typename(usi) , sizeof(usi),
    get_name(ui)  , typename(ui)  , sizeof(ui) ,
    get_name(uli) , typename(uli) , sizeof(uli),
    get_name(ulli), typename(ulli), sizeof(ulli)); 

  unsigned short int     *usip ;
  unsigned int           *uip  ;
  unsigned long int      *ulip ;
  unsigned long long int *ullip;
  printf("UNSIGNED INT POINTEr::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s  ]\t[ %ld ]\n"
    "\t%s  \t[ %s    ]\t[ %ld ]\n"
    "\t%s  \t[ %s  ]\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t[ %ld ]\n\n",
    get_name(usip) , typename(usip) , sizeof(usip),
    get_name(uip)  , typename(uip)  , sizeof(uip) ,
    get_name(ulip) , typename(ulip) , sizeof(ulip),
    get_name(ullip), typename(ullip), sizeof(ullip)); 



  /*FLOAT-DOUBLE*/
  float_t  f_t = 0.1f;
  double_t d_t = 0.1f;
  printf("FLOAT_T / DOUBLE_T::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t\t[ %ld ]\n\n",
    get_name(f_t), typename(f_t), sizeof(f_t),
    get_name(d_t), typename(d_t), sizeof(d_t)); 
     
  float_t  f_tp;
  double_t d_tp;
  printf("FLOAT_T / DOUBLE_T POINTER::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s  ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t\t[ %ld ]\n\n",
    get_name(f_tp), typename(f_tp), sizeof(f_tp),
    get_name(d_tp), typename(d_tp), sizeof(d_tp));  
     

  float       f  = 0.1f;
  double      d  = 0.1f;
  long double ld = 0.1f;
  printf("FLOAT / DOUBLE::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s    ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s   ]\t\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t\t[ %ld ]\n\n",
    get_name(f) , typename(f) , sizeof(f),
    get_name(d) , typename(d) , sizeof(d),
    get_name(ld), typename(ld), sizeof(ld)); 
    
  float       *fp ;
  double      *dp ;
  long double *ldp;
  printf("FLOAT / DOUBLE POINTER::\n"
    "\tname\ttype\n"
    "\t%s  \t[ %s    ]\t[ %ld ]\n"
    "\t%s  \t[ %s   ]\t[ %ld ]\n"
    "\t%s  \t[ %s ]\t[ %ld ]\n\n",
    get_name(fp) , typename(fp) , sizeof(fp),
    get_name(dp) , typename(dp) , sizeof(dp),
    get_name(ldp), typename(ldp), sizeof(ldp)); 
    printf("\tFLT_EVAL_METHOD :: %d\n", FLT_EVAL_METHOD);
  #endif //TEST_VAR_TYPES

  
  return 0;
}
