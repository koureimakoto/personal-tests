/*
 Author::name("Talles H");
 License::type("HaveFun");
 */

#include <stdio.h>


/* Jump table */
#define FUNC G1:
#define CELL G2:
#define ALPH G3:
#define DIGT G4:
#define END  G5:

/* Define simple init */
#define JMP static void *go[] = {&&G1, &&G2, &&G3, &&G4, &&G5 };


/* Jump ID */
enum list
  {
  func = 0,
  cell = 1,
  alph = 2,
  digt = 3,
  end  = 4
  };


int
main(void)
{
  /* Init Goto Array */
  JMP
  
  /* Jump to : */ 
goto *go[alph];
  FUNC  printf("goto FUNCTION -1\n");
  
goto *go[end]; // Comm
  CELL  printf("goto CELL -2\n");
      
goto *go[end];
  ALPH  printf("goto ALPHA -3\n");
  
goto *go[end];
  DIGT  printf("goto DIGIT -4\n");
  
  
  END  printf("goto END -5\n");
  
  /* Final */
  printf("[func = cell]:: %p\n", go[0] + 0x1a);
  printf("[ cell ]:: %p\n", go[1]);
  printf("[ alph ]:: %p\n", go[2]);
  printf("[ digt ]:: %p\n", go[3]);
  printf("[ end  ]:: %p\n", go[4]);   
  return 0;
}






