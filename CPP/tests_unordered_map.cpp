/*
 Author::name("Talles H");
 License::type("HaveFun");
 */


#include <iostream>
#include <unordered_map>
#include <cstdint>

typedef int32_t ml_id_t;

/* Teste de redefiniçaão de tipo  */
template<typename Y>
using type_map = std::unordered_map<ml_id_t, Y>;


/* Mapa exemplo <id, tipo> */
type_map<int32_t> mapinha;


/* função simulação do retorno para iterator do insert do unordered_map. */
std::pair<type_map<int32_t>::iterator,bool>
u32 ( int32_t id, uint32_t in )
    { return mapinha.insert( std::pair<int, int>(id, in) ); }


/* Estrutura basica para controle das variaveis no compilador */
typedef struct
hash_pointer{

  int32_t flag :  8;
  int32_t level:  8;
  int32_t type :  8;
  int32_t id   : 32;
  void * ptr;

} HASH_PTR;


int 
main()
{
    /* Mapa principal das variaveis do compilador */
    std::unordered_map<std::string, HASH_PTR > hash_map;
    
    /* std::pair<std::unordered_map<int32_t, int32_t>::iterator, bool> */
    /* Insercao no mapa por tipo usando fundao de nome primeira_letra|tipo|tamanho_em_bits */
    auto it = u32(150, 99);
    
    /* Insercao do no mapa princiapal */
    hash_map.insert( 
        std::pair<std::string, HASH_PTR>(
            "oi",       // Nome da variavel
            {
                0,      // Flags - Static, private, public, protected...
                1,      // Scope
                2,      // Token for type
                150,    // id - Redundance
                &(it.first->second)
            }
        )
    );
    
    /* Saida  */
    std::cout << "Valor que ficará no mapeamento do int ::"    << it.first->second <<  '\n';
    std::cout << "Endereco do valor que ficará no hash_map ::" << &it.first->second << '\n';
    std::cout << "Valor resgatado pelo hash_map :: "           << *((int *) hash_map["oi"].ptr);
    
    return 0;
}

