/*
 Author::name("Talles H");
 License::type("HaveFun");
 */


#include <iostream>
#include <time.h>


class 
test_stmt_perform
{
public:
  long int
    k = 0,  //Bah
    z = 0;

  // Test com IF agrupado
  int test_if_group (unsigned int var)
    {
    if (var < 6)
      {
	  if (var == 1)
	    z++;
	  else if (var == 2)
	    z--;
	  else if (var == 3)
	    z--;
	  else if (var == 4)
	    z--;
	  else
	    z--;
      }
    else if (var < 11)
      {
	  if (var == 6)
	    z--;
	  else if (var == 7)
	    z--;
	  else if (var == 8)
	    z--;
	  else if (var == 9)
	    z--;
	  else
	    z--;
      }
    else if (var < 14)
      {
	  if (var == 11)
	    z--;
	  else if (var == 12)
	    z--;
	  else
	    z--;
      }
      
    if (var == 14)
      z--;
    else if (var == 15)
      z--;
    else if (var == 16)
      z--;
    else if (var == 17)
      z--;
    else if (var == 18)
      z++;
    else if (var == 19)
      z--;
    else if (var == 20)
      z--;
    else if (var == 21)
      z--;
    else if (var == 22)
      z++;
    }

  // Teste com Array de Goto
  int
  test_goto (unsigned int var)
    {
    void *go[] =
      {
      &&go1,  &&go2,  &&go3,  &&go4,  &&go5,  &&go6,  &&go7,  &&go8,  &&go9,
      &&go10, &&go11, &&go12, &&go13, &&go14, &&go15, &&go16, &&go17, &&go18,
      &&go19, &&go20, &&go21, &&go22
      };

    goto *go[var];
    go1:
      k++;
      goto end;
    go2:
      k++;
      goto end;
    go3:
      k++;
      goto end;
    go4:
      k++;  
      goto end;
    go5:
      k++;
      goto end; 
    go6:
      k++;
      goto end;
    go7:
      k++;
      goto end;
    go8:
      k++;
      goto end;
    go9:
      k++;
      goto end;
    go10:
      k--;
      goto end;
    go11:
      k++;
      goto end;
    go12:
      k++;
      goto end;
    go13:
      k++;
      goto end;
    go14:
      k++;
      goto end;
    go15:
      k++;
      goto end;
    go16:  
      k++;
      goto end;
    go17:
      k++;  
      goto end;
    go18:
      k--;
      goto end;
    go19:  
      k++;
      goto end;
    go20:
      k++;
      goto end;
    go21:
      k++;
      goto end;
    go22:  
      k++;
      goto end;

    end:
      return 0;
    }

  // Teste com if sequencial
  int
  test_if_seq (unsigned int var)
    {
    if (var == 1)
      z++;
    else if (var == 2)
      z--;
    else if (var == 3)
      z--;
    else if (var == 4)
      z--;
    else if (var == 5)
      z--;
    else if (var == 6)
      z--;
    else if (var == 7)
      z--;
    else if (var == 8)
      z--;
    else if (var == 9)
      z--;
    else if (var == 10)
      z--;
    else if (var == 11)
      z--;
    else if (var == 12)
      z--;
    else if (var == 13)
      z--;
    else if (var == 14)
      z--;
    else if (var == 15)
      z--;
    else if (var == 16)
      z--;
    else if (var == 17)
      z--;
    else if (var == 18)
      z++;
    else if (var == 19)
      z--;
    else if (var == 20)
      z--;
    else if (var == 21)
      z--;
    else if (var == 22)
      z++;
    
    return 0;
    }


  // Teste com switch - Jump Table
  int 
  test_switch (unsigned int var)
    {
    switch (var)
      {
      case 1:
	    z++;
	    break;
      case 2:
	    z--;
	    break;
      case 3:
	    z--;
	    break;
      case 4:
	    z--;
	    break;
      case 5:
	    z--;
	    break;
      case 6:
	    z--;
	    break;
      case 7:
	    z--;
	    break;
      case 8:
	    z--;
	    break;
      case 9:
	    z--;
	    break;
      case 10:
	    z--;
	    break;
      case 11:
	    z--;
	    break;
      case 12:
	    z--;
	    break;
      case 13:
	    z--;
	    break;
      case 14:
	    z--;
	    break;
      case 15:
	    z--;
	    break;
      case 16:
	    z--;
	    break;
      case 17:
	    z--;
	    break;
      case 18:
	    z--;
	    break;
      case 19:
	    z--;
	    break;
      case 20:
	    z--;
	    break;
      case 21:
	    z--;
	    break;
      case 22:
	    z--;
	    break;
      }
    return 0;
    }
};

#define MELHOR_CASO 1       // Best case
#define PIOR_CASO  22       // Worst case
#define MEIO_CASO  11		// Middle

int
main (int argc, char *argv[])
{
  const unsigned int
    NUM_ELEMENTS = 900000000,   // Cycles
    TEST_VALUE   = MELHOR_CASO; // Test 
  
  unsigned int
    old,    // Time
    count;  // Loop
  
  long double
    secs;

  test_stmt_perform
    test;   // Class

  /* Teste IF Agrupado. */
  old = clock ();
  for (count = 0; count < NUM_ELEMENTS; count++)
    {
      test.test_if_group (TEST_VALUE);
    }

  old  = clock () - old;
  secs = ((long double) old) / CLOCKS_PER_SEC;
  std::cout << "TIME::IF(G) ::" << secs << " seconds)\n";
  /* Fim do teste. */



  /* Teste IF sequencial */
  old = clock ();
  for (count = 0; count < NUM_ELEMENTS; count++)
    {
      test.test_if_seq (TEST_VALUE);
    }

  old  = clock () - old;
  secs = ((long double) old) / CLOCKS_PER_SEC;
  std::cout << "TIME::IF(S) ::" << secs << " seconds)\n";
  /* Fim do teste. */



  /* Teste Array de Goto */
  old = clock ();
  for (count = 0; count < NUM_ELEMENTS; count++)
    {
      test.test_goto (TEST_VALUE-1);
    }

  old  = clock () - old;
  secs = ((long double) old) / CLOCKS_PER_SEC;
  std::cout << "TIME::GOTO ::" << secs << " seconds)\n";
  /* Fim do teste. */



  /* Teste switch - Jump Table */
  old = clock ();
  for (count = 0; count < NUM_ELEMENTS; count++)
    {
      test.test_switch (TEST_VALUE);
    }

  old  = clock () - old;
  secs = ((long double) old) / CLOCKS_PER_SEC;
  std::cout << "TIME::SWCH ::" << secs << " seconds)\n";
  /* Fim do teste. */

  return 0;
}

