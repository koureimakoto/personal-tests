<?php
/*
 Author::name("Talles H");
 License::type("HaveFun");
 */

/* Test : include vs require time to call */

if(true)
  {/* INCLUDE */
  echo '<BR>INCLUDE<BR>';
  $init = microtime(TRUE);
  for($i = 0 ; $i <= 100 ; $i++)
    include "test/test$i.php";

  $end = microtime(TRUE) - $init;
  echo "<br> TEMPO FINAL :: $end";
  }
else
  {/* REQUIRE */
  echo '<BR>REQUIRE<BR>';
  $init = microtime(TRUE);
  for($i = 0 ; $i <= 100 ; $i++)
    require "test/test$i.php";

  $end = microtime(TRUE) - $init;
  echo "<br> TEMPO FINAL :: $end";
  }

?>
